var app = angular.module('BeBeer');

app.controller('login-ctrl', function($location, $route, beerFactory){
  let login = this;
  
  login.username = "";

  function inside(data){
    beerFactory.setIsLog(true);
    beerFactory.setUsername(login.username);
    beerFactory.setUserBeers(data);
    // Reload por rendimiento
    $route.reload();
    $location.path('home');
  }

  login.enter = (e)=>{
    if (e.which == 13 && login.username !== ""){
      firebase.database().ref(`/${login.username}/`).once('value').then((snapshot)=> {
        let result = snapshot.val()
        if(result == null){
          firebase.database().ref(`/${login.username}/`).set({0:0}).then(()=>{
            inside({0:0});
          });
        }else{
          let userbeersd = {}
          for(let k in result){
            userbeersd[k] = result[k]
          }
          inside(userbeersd);
        }
      });
    }
}

});