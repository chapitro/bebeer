var app = angular.module('BeBeer');

app.controller('home-ctrl', function($http, $location, beerFactory){
	
	if(!beerFactory.getIsLog()){
		$location.path('/');
	}
	
	let home = this;

	home.username = beerFactory.getUsername();
	home.userbeers = beerFactory.getUserBeers();

	home.page = 1
	home.busy = false;
	
	/**
	 * Este método se encarga de solicitar la siguiente página a Punk API,
	 * si esta devuelve una lista vacía es porque ya no hay más resultados
	 * por mostrar y por ende se deja el estado busy en true para que no intente
	 * realizar más solicitudes.
	 */
	home.nextPage = ()=>{
		if (this.busy) return;
		this.busy = true;

		$http({
			method: 'GET',
			url: `https://api.punkapi.com/v2/beers?page=${home.page}`
		}).then( (response)=> {
			if(response.data != []){
				home.data = home.data.concat(response.data);
				home.page += 1;
				home.busy = false;
				if(home.page == 2){
					home.nextPage()
				}
			}
			else{
				// Indica que ya no hay más resultados en Punk API
				home.busy = true;
			}
		});
	}

	home.data = []

	home.addBeer = (idBeer)=>{
		let previo = (home.userbeers[idBeer] !== undefined)?home.userbeers[idBeer]:0;
		let key = `/${home.username}/${idBeer}/`
		let value = previo+1
		let update = {}
		update[key] = previo+1
		firebase.database().ref().update(update);
		if(home.userbeers[idBeer] == undefined){
			home.userbeers[idBeer] = value
		}else{
			home.userbeers[idBeer] = value
		}
		home.getBestBeers();
	}

	home.startZero = false
	home.bestBeers = []

	home.getBestBeers = ()=>{
		// Convertir a lista [a,b]
		var keyValues = []
		for (var key in home.userbeers) {
			keyValues.push([ key, home.userbeers[key] ])
		}
		// Ordenar
		keyValues.sort(function compare(kv1, kv2) {
			return kv1[1] - kv2[1]
		})
		// Revertir
		keyValues = keyValues.reverse();
		// Seleccionar mejores
		let ids = []
		if(keyValues.length <= 5){
			keyValues.forEach((e)=>{
				ids.push(e[0])
			})
		}else{
			for(let i=0; i<5;i++){
				ids.push(keyValues[i][0])
			}
		}
		home.bestBeers = []
		if(ids.length > 1){
			home.startZero = true
		}
		ids.forEach((e)=>{
			if(e==0)return
			$http({
				method: 'GET',
				url: `https://api.punkapi.com/v2/beers/${e}`
			}).then( (response)=> {
				if(!("error" in response.data)){
					home.bestBeers = home.bestBeers.concat(response.data)
				}
			});
		})
	}

	home.getBestBeers();
});