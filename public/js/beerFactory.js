var app = angular.module("BeBeer");

app.factory("beerFactory",function(){
	var factory = {};

	factory.isLog = false;
	factory.getIsLog = ()=> factory.isLog;
	factory.setIsLog = (isLog)=> {
		factory.isLog = isLog;
		return factory.isLog;
	};

	factory.username = {};
	factory.getUsername = ()=> factory.username;
	factory.setUsername = (username)=> {
		factory.username = username;
		return factory.username;
	};

	factory.userBeers = {};
	factory.getUserBeers = ()=> factory.userBeers;
	factory.setUserBeers = (userBeers)=> {
		factory.userBeers = userBeers;
		return factory.userBeers;
	};
	
	return factory;
});