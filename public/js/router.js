var app = angular.module('BeBeer');
app.config(function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl: '/home.html'
	}).when('/', {
		templateUrl: '/login.html'
	});
});