'use strict';

// Libraries
const
	express = require('express'),
	path = require('path');

// Constants
const PORT =  process.env.PORT || 8040;

// Server
const app = new express();

// Setup static files
app.use(express.static(path.join(__dirname, 'public')));

// Run server
app.listen(PORT, ()=>{
	console.log(`Listen http://localhost:${PORT}`);
});

